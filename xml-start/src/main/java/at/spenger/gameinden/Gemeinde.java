package at.spenger.gameinden;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;

public class Gemeinde {
	
	@XmlSchemaType(name = "positiveInteger")
    protected BigInteger adresscode;
    @XmlElement(required = true)
    protected String aktualdat;
    @XmlElement(required = true)
    protected String bgmname;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(required = true)
    protected String gemfax;
    @XmlElement(required = true)
    protected String gemname;
    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger gemnr;
    protected String gemtel;
    @XmlElement(required = true)
    protected String oedomain;
    @XmlElement(required = true)
    protected String ort;
    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger plz;
    protected String strasse;
    @XmlElement(required = true)
    protected String vbgmname;
    @XmlElement(required = true)
    protected String webadr;
    
    
    
    
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((adresscode == null) ? 0 : adresscode.hashCode());
		result = prime * result
				+ ((aktualdat == null) ? 0 : aktualdat.hashCode());
		result = prime * result + ((bgmname == null) ? 0 : bgmname.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((gemfax == null) ? 0 : gemfax.hashCode());
		result = prime * result + ((gemname == null) ? 0 : gemname.hashCode());
		result = prime * result + ((gemnr == null) ? 0 : gemnr.hashCode());
		result = prime * result + ((gemtel == null) ? 0 : gemtel.hashCode());
		result = prime * result
				+ ((oedomain == null) ? 0 : oedomain.hashCode());
		result = prime * result + ((ort == null) ? 0 : ort.hashCode());
		result = prime * result + ((plz == null) ? 0 : plz.hashCode());
		result = prime * result + ((strasse == null) ? 0 : strasse.hashCode());
		result = prime * result
				+ ((vbgmname == null) ? 0 : vbgmname.hashCode());
		result = prime * result + ((webadr == null) ? 0 : webadr.hashCode());
		return result;
	}
	public String getWebadr() {
		return webadr;
	}
	public void setWebadr(String webadr) {
		this.webadr = webadr;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gemeinde other = (Gemeinde) obj;
		if (adresscode == null) {
			if (other.adresscode != null)
				return false;
		} else if (!adresscode.equals(other.adresscode))
			return false;
		if (aktualdat == null) {
			if (other.aktualdat != null)
				return false;
		} else if (!aktualdat.equals(other.aktualdat))
			return false;
		if (bgmname == null) {
			if (other.bgmname != null)
				return false;
		} else if (!bgmname.equals(other.bgmname))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (gemfax == null) {
			if (other.gemfax != null)
				return false;
		} else if (!gemfax.equals(other.gemfax))
			return false;
		if (gemname == null) {
			if (other.gemname != null)
				return false;
		} else if (!gemname.equals(other.gemname))
			return false;
		if (gemnr == null) {
			if (other.gemnr != null)
				return false;
		} else if (!gemnr.equals(other.gemnr))
			return false;
		if (gemtel == null) {
			if (other.gemtel != null)
				return false;
		} else if (!gemtel.equals(other.gemtel))
			return false;
		if (oedomain == null) {
			if (other.oedomain != null)
				return false;
		} else if (!oedomain.equals(other.oedomain))
			return false;
		if (ort == null) {
			if (other.ort != null)
				return false;
		} else if (!ort.equals(other.ort))
			return false;
		if (plz == null) {
			if (other.plz != null)
				return false;
		} else if (!plz.equals(other.plz))
			return false;
		if (strasse == null) {
			if (other.strasse != null)
				return false;
		} else if (!strasse.equals(other.strasse))
			return false;
		if (vbgmname == null) {
			if (other.vbgmname != null)
				return false;
		} else if (!vbgmname.equals(other.vbgmname))
			return false;
		if (webadr == null) {
			if (other.webadr != null)
				return false;
		} else if (!webadr.equals(other.webadr))
			return false;
		return true;
	}
	public BigInteger getAdresscode() {
		return adresscode;
	}
	public void setAdresscode(BigInteger adresscode) {
		this.adresscode = adresscode;
	}
	public String getAktualdat() {
		return aktualdat;
	}
	public void setAktualdat(String aktualdat) {
		this.aktualdat = aktualdat;
	}
	public String getBgmname() {
		return bgmname;
	}
	public void setBgmname(String bgmname) {
		this.bgmname = bgmname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGemfax() {
		return gemfax;
	}
	public void setGemfax(String gemfax) {
		this.gemfax = gemfax;
	}
	public String getGemname() {
		return gemname;
	}
	public void setGemname(String gemname) {
		this.gemname = gemname;
	}
	public BigInteger getGemnr() {
		return gemnr;
	}
	public void setGemnr(BigInteger gemnr) {
		this.gemnr = gemnr;
	}
	public String getGemtel() {
		return gemtel;
	}
	public void setGemtel(String gemtel) {
		this.gemtel = gemtel;
	}
	public String getOedomain() {
		return oedomain;
	}
	public void setOedomain(String oedomain) {
		this.oedomain = oedomain;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	public BigInteger getPlz() {
		return plz;
	}
	public void setPlz(BigInteger plz) {
		this.plz = plz;
	}
	public String getStrasse() {
		return strasse;
	}
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	public String getVbgmname() {
		return vbgmname;
	}
	public void setVbgmname(String vbgmname) {
		this.vbgmname = vbgmname;
	}

	
	
	
	
}

