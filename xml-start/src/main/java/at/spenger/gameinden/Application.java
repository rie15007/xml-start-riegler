package at.spenger.gameinden;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import at.spenger.xml.shiporder.Item;
import at.spenger.xml.shiporder.StaXParser;

public class Application {

	
	public static void main (String [] args) throws IOException
	{
		readGemeinden();
	}
	
	private static void readGemeinden() throws IOException {
		InputStream in = new ClassPathResource("Gemeinden.xml")
				.getInputStream();
		StaXParser p = new StaXParser();

		List<Item> items = p.read(in);

		for (Item item : items) {
			System.out.println(item);
		}
	}
}
