package at.spenger.gameinden;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

public class StaXParser {

	public List<Gemeinde> read(InputStream in) throws IOException {
		final String ITEM = "item";
		List<Gemeinde> buecher = new ArrayList<>();

		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document
			Gemeinde item = null;
			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {

					switch (ev.asStartElement().getName().getLocalPart()) {
					case ITEM:
						// If we have an item element, we create a new item
						item = new Gemeinde();
						break;
					case "adresscode":
						ev = eventReader.nextEvent();
						item.setAdresscode(new BigInteger(ev.asCharacters().getData()));
						continue;
					case "aktualdat":
						ev = eventReader.nextEvent();
						item.setAktualdat(ev.asCharacters().getData());
						continue;
					case "bgmname":
						ev = eventReader.nextEvent();
						item.setBgmname(ev.asCharacters().getData());
						continue;
					case "email":
						ev = eventReader.nextEvent();
						item.setEmail(ev.asCharacters().getData());
						continue;
					case "gemfax":
						ev = eventReader.nextEvent();
						item.setGemfax(ev.asCharacters().getData());
						continue;
					case "gemname":
						ev = eventReader.nextEvent();
						item.setGemname(ev.asCharacters().getData());
						continue;
					case "gemnr":
						ev = eventReader.nextEvent();
						item.setGemnr(new BigInteger(ev.asCharacters().getData()));

						continue;
					case "gemtel":
						ev = eventReader.nextEvent();
						item.setGemtel(ev.asCharacters().getData());
						continue;
					case "oedomain":
						ev = eventReader.nextEvent();
						item.setOedomain(ev.asCharacters().getData());
						continue;
					case "ort":
						ev = eventReader.nextEvent();
						item.setOrt(ev.asCharacters().getData());
						continue;
					case "plz":
						ev = eventReader.nextEvent();
						item.setPlz(new BigInteger(ev.asCharacters().getData()));

						continue;
					case "strasse":
						ev = eventReader.nextEvent();
						item.setStrasse(ev.asCharacters().getData());
						continue;
					case "vbgmname":
						ev = eventReader.nextEvent();
						item.setVbgmname(ev.asCharacters().getData());
						continue;
					case "webadr":
						ev = eventReader.nextEvent();
						item.setWebadr(ev.asCharacters().getData());
						continue;
					}
				}
				if (ev.isEndElement()) {
					if (ev.asEndElement().getName().getLocalPart()
							.equals(ITEM)) {
						buecher.add(item);
					}

				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return buecher;
	}

	public void readSimple(InputStream in) throws IOException {
		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		Stack<String> stck = new Stack<String>();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {
					stck.push(ev.asStartElement().getName().getLocalPart());

					@SuppressWarnings("unchecked")
					Iterator<Attribute> iter = ev.asStartElement()
					.getAttributes();
					while (iter.hasNext()) {
						Attribute a = iter.next();
						System.out.println(buildXPathString(
								stck,
								"/@" + a.getName().getLocalPart() + "=\""
										+ a.getValue() + "\""));
					}
				}
				if (ev.isCharacters()) {
					String s = ev.asCharacters().getData();
					if (s.trim().length() > 0)
						System.out.println(buildXPathString(stck, "=\"" + s
								+ "\""));
				}
				if (ev.isEndElement())
					stck.pop();

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String buildXPathString(Stack<String> stck, String postfix) {
		StringBuffer sb = new StringBuffer();
		for (String s : stck)
			sb.append("/").append(s);
		sb.append(postfix);
		return sb.toString();
	}

}
